1] This is a sample User management project which perfrom create, retrive, modify, delete users operations on user(CRUD operations)
2] Request takes the input in the form of xml
3] Following operations are supported with the syntax:

#Retrive User Record:
URL: http://localhost:8080/ManageUsers/rest/user/get/<user_id>
HTTP Method : GET 
Response : 
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<user userId="<user_id>">
    <address><user address></address>
    <firstName><users first name></firstName>
    <lastName><users last name></lastName>
    <profile><users job profile></profile>
</user>

#Create User Record:
URL: http://localhost:8080/ManageUsers/rest/user/create
HTTP Method: POST
Input body: 
<user userId="<user_id>">
    <address><user address></address>
    <firstName><users first name></firstName>
    <lastName><users last name></lastName>
    <profile><users job profile></profile>
</user>

Response:
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<result>
    <errorCode>USER_CREATED</errorCode>
    <message>User with userId [<user_id>] CreateUser successfully</message>
    <success>true</success>
</result>

#Update User Record:
URL: http://localhost:8080/ManageUsers/rest/user/update
HTTP Method: PUT
Input body: 
<user userId="<user_id>">
    <address><user address></address>
    <firstName><users first name></firstName>
    <lastName><users last name></lastName>
    <profile><users job profile></profile>
</user>

Response:
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<result>
    <errorCode>USER_UPDATED</errorCode>
    <message>User with userId [<user_id>] UpdateUser successfully</message>
    <success>true</success>
</result>

#Delete User Record:
URL: http://localhost:8080/ManageUsers/rest/user/delete/<user_id>
HTTP Method : DELETE
Response : 
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<result>
    <errorCode>DELETE_USER</errorCode>
    <message>User with userId [<user_id>] deleted from the system.</message>
    <success>true</success>
</result>

4] To make the program running you need to have database in a system. After creating a database you need to update hibernate.hbm.xml file for <DATABASE_USER>, <DATABASE_USER_PASSWORD>, <LISTNER_PORT>, <DATABASE_NAME> 
5] After creating a database, execute follwoing sql query to create user table:

create table MUSER (
    id varchar(40) primary key,
	user_id varchar2(20) not null unique,
	first_name varchar2(20) not null,
	last_name varchar2(20) not null,
	address varchar2(20) not null,
	profile varchar2(20) not null
);

5] The length of the string value for users first name, last name, address, profile should not exceed 20. If it is more than the specified then it throws following error message
Error response:

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<result>
    <errorCode>XML_VALIDATION_FAILED</errorCode>
    <message>XML input values exceeded the limit of characters. The number of characters for FirstName, LastName, Address, Profile should not exceed 20</message>
    <success>false</success>
</result>

6] If ssl is enabled on the server side then URL host will change to "https://localhost:8443", rest of the part will remain same
7] To enable ssl on tomcat we can refer the steps mentioned in following link:
https://tomcat.apache.org/tomcat-8.0-doc/ssl-howto.html

HTTPS is SSL over HTTP where server side public certificated gets authenticated by the client. To get this done, on the client side's trustore there must be the public certificated of the server.

7]Basic authentication can be enabled on the tomcat by modifying the following files
In web.xml of the application we need to add following lines at the end:

.....................
<security-constraint>
		<web-resource-collection>
			<web-resource-name>Wildcard means whole app requires authentication</web-resource-name>
			<url-pattern>/rest/*</url-pattern>
			<http-method>GET</http-method>
			<http-method>POST</http-method>
			<http-method>PUT</http-method>
			<http-method>DELETE</http-method>
		</web-resource-collection>
 
      	<auth-constraint>
        	<role-name>tomcat</role-name>
      	</auth-constraint>
</security-constraint>
	
	<login-config>
		<auth-method>BASIC</auth-method>
	</login-config>
	
	<security-role>
     	<description>Normal operator user</description>
     	<role-name>tomcat</role-name>
   	</security-role>
..................... 

And in the tomcat-users.xml file of the tomcat server, we need to add following lines inside <tomcat-users> tag:
..................
<role rolename="tomcat"/>
<user username="tomcat" password="tomcat" roles="tomcat"/>
..................

Implementation details:
1] Here the object which perfrom a CRUD operation on user is implemented as a Service. Service is process which starts when the application server starts and can be availed from any part of the application.
2] Hibernate is used to perfrom CRUD operations on the user with the database.
3] JaxRS is been used to expose the API as rest service
4] JaxB is used for the xml validation, which validates the incomming xml request against the xsd schema
5] Client HttpClient program written. You can uncomment the main() in this class to test the rest service.
6] manage-services.xml file is a file where the services is been specified for the intitalization