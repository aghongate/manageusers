package com.storage;

import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public final class PersistanceManager {

	private SessionFactory factory = new Configuration().configure().buildSessionFactory();;
	
	
	private static PersistanceManager manager = null;
	
	private PersistanceManager(){
		
	}
	
	private SessionFactory getFactory() {
		return this.factory;
	}

	// singleton object of PersistanceManager
	public static PersistanceManager getInstance(){
		if (manager == null){
			synchronized(PersistanceManager.class){
				if (manager == null){
					manager = new PersistanceManager();
				}
			}
		}
		
		return manager;
	}
	
	public void createOrUpdate(PersistanceObject obj) throws PersistenceException{
		Session session = null;
		try{
			session = getInstance().getFactory().openSession();
			session.beginTransaction();
			session.saveOrUpdate(obj);
			session.getTransaction().commit();
		} catch(Exception e){
			session.getTransaction().rollback();
			throw new PersistenceException("Unable to save Object", e);
		} finally{
			session.close();
		}
	}
	
	public List executeQuery(String query){
		Session session = null;
		try{
			session = getInstance().getFactory().openSession();
			session.beginTransaction();
			Query q = session.createQuery(query);
			return q.list();
		} catch(Exception e){
			session.getTransaction().rollback();
			throw new PersistenceException("Unable to save Object", e);
		} finally{
			session.close();
		}
		
	}
	
	public void delete(PersistanceObject obj) throws PersistenceException{
		Session session = null;
		try{
			session = getInstance().getFactory().openSession();
			session.beginTransaction();
			session.delete(obj);
			session.getTransaction().commit();
		} catch(Exception e){
			session.getTransaction().rollback();
			throw new PersistenceException("Unable to delete Object", e);
		} finally{
			session.close();
		}
	}
	
	public List<PersistanceObject> execiteCriteria(Class objClass, Map<String, String> criterias) throws PersistenceException{
		Session session = null;
		try{
			session = getInstance().factory.openSession();
			Criteria criteria = createCriteria(objClass, session, criterias);
			session.beginTransaction();
			List<PersistanceObject> list = (List<PersistanceObject>)criteria.list();
			return list;
		} catch(Exception e){
			session.getTransaction().rollback();
			throw new PersistenceException("Unable to execute criteria", e);
		} finally{
			session.close();
		}
	}
	
	private static Criteria createCriteria(Class objClass, Session session, Map<String, String> criterias){
		Criteria criteria = session.createCriteria(objClass);

		if (criterias != null){
			for (Map.Entry<String, String> entry : criterias.entrySet()){
				criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
			}
		}
		return criteria;
		
	}
	
}
