package com.storage;

import java.util.Map;

public class PersistanceObject {

	// Extending object may perform operation on itself. Future support 
	protected void insertRecord(PersistanceObject obj){
		PersistanceManager.getInstance().createOrUpdate(obj);
	}
	
	protected void updateRecord(PersistanceObject obj){
		PersistanceManager.getInstance().createOrUpdate(obj);
	}
	
	protected PersistanceObject getRecord(Class objClass, Map<String, String> criterias){
		return (PersistanceObject) PersistanceManager.getInstance().execiteCriteria(objClass, criterias);
		
	}
	
	protected void deleteRecord(Class objClass, Map<String, String> criterias){
		PersistanceObject obj = this.getRecord(objClass, criterias);
		
		if (obj != null){
			PersistanceManager.getInstance().delete(obj);
		}
	}
	
	
}
