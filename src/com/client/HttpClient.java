package com.client;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.log4j.Logger;
import com.sun.org.apache.xml.internal.security.utils.Base64;


public class HttpClient {

	// Currently URL is been hard-coded. This also can be sent as a parameter to the execute<Method>() methods
	private static final String HOST_URL="http://localhost:8443";
	private static final String ROOT="/ManageUsers";
	private static final String GET_URL= HOST_URL + ROOT + "/rest/user/get/";
	private static final String POST_URL=HOST_URL + ROOT + "/rest/user/create";
	private static final String PUT_URL=HOST_URL + ROOT + "/rest/user/update";
	private static final String DELETE_URL=HOST_URL + ROOT + "/rest/user/delete/";
	private static final String PUBLIC_CERT_PATH="<path of the server certificate>"; // Need to download and create a certificate file in der format(plain text certificate) from the server site and put the path of that file here
	private static final String USER_CERT_ALIAS = "cert_";
	private static final Logger log = Logger.getLogger(HttpClient.class);
	
	public String executeGet(String username, String password, String resourceId){

		String encoded = Base64.encode((username + ":" + password).getBytes());
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("Authorization", "Basic " + encoded);
		Request request = new Request();
		request.setUrl(GET_URL + resourceId)
		.setMethod("GET")
		.setRequestParam(requestParams);
		String response = this.execute(request);
		System.out.println(response);
		return response;
		
	}

	public String executePost(String username, String password, String body){
		String encoded = Base64.encode((username + ":" + password).getBytes());
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("Authorization", "Basic " + encoded);
		Request request = new Request();
		request.setUrl(POST_URL)
		.setRequestParam(requestParams)
		.setMethod("POST")
		.setBody(body);
		
		return this.execute(request);
	}

	public String executePut(String username, String password, String body){
		String encoded = Base64.encode((username + ":" + password).getBytes());
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("Authorization", "Basic " + encoded);
		Request request = new Request();
		request.setUrl(PUT_URL)
		.setRequestParam(requestParams)
		.setMethod("PUT")
		.setBody(body);
		
		return this.execute(request);

	}
	
	public String executeDelete(String username, String password, String resourceId){
		String encoded = Base64.encode((username + ":" + password).getBytes());
		Map<String, String> requestParams = new HashMap<String, String>();
		requestParams.put("Authorization", "Basic " + encoded);
		Request request = new Request();
		request.setUrl(DELETE_URL + resourceId)
		.setRequestParam(requestParams)
		.setMethod("DELETE");
		
		return this.execute(request);

	}

	public String execute(Request request){
		URL url = null;
		HttpsURLConnection connection = null; // Need to change HttpUrlConnectino id URL is not secure
		// this will not check whether the domain in the certificate and the domain from where the certificate is coming during ssl handshake are same or not
		HostnameVerifier hv = new HostnameVerifier() {
			@Override
            public boolean verify(String urlHostName, SSLSession session) {
                return true;
            }
        };
        
		try {
			url = new URL(request.getUrl());
			connection.setSSLSocketFactory(getSSLContext(readCertificate(null)).getSocketFactory()); // null is passed so as to use the default path
			connection = (HttpsURLConnection)url.openConnection();
			
			for (Map.Entry<String, String> entry : request.getRequestParam().entrySet()){
				connection.setRequestProperty(entry.getKey(), entry.getValue());
			}

			
    		connection.setRequestMethod(request.getMethod());
    		connection.setDoOutput(true);
    		connection.setDoInput(true);
            connection.setRequestProperty("accept", "application/xml");
         
			String body = request.getBody();
			if (body == null){
				body = "";
			}
			if (request.getMethod().equals("POST") || request.getMethod().equals("PUT")){
				connection.setRequestProperty("Content-Type", "application/xml");
				connection.getOutputStream().write(body.getBytes());
			}
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				System.out.println("Response code: " + connection.getResponseCode());
				System.out.println("Response is:\n");
				String inputLine = null;
				while ((inputLine = br.readLine()) != null){
					System.out.println(inputLine);
				}
			} catch (IOException e){
				return "fail";
			}
		
			if (connection != null && (connection.getResponseCode() == 201) 
					               || (connection.getResponseCode() == 200)
					               || (connection.getResponseCode() == 302)){
				return "Success";
			} else {
				return "fail";
			}
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "fail";
		} catch (IOException e){
			e.printStackTrace();
			return "failed during read-write operation";
		} catch (Exception e){
			e.printStackTrace();
			return "fail";
		}
		
		
	}
	

	private String readCertificate(String certificatePath) throws Exception{
		if (certificatePath == null){
			certificatePath = PUBLIC_CERT_PATH;
		}
		File certFile = new File(certificatePath);
		StringBuffer sb = new StringBuffer();
		try {
		BufferedReader br = new BufferedReader(new FileReader(certFile));
		String line = br.readLine();
		while(line != null){
			sb.append(line);
			line = br.readLine();
		}
		} catch (IOException ioExcep){
			throw new Exception("Unable to read certificate file", ioExcep);
		}
		return sb.toString();
		
	}
	
	public static SSLContext getSSLContext(String certStr) throws Exception {
        SSLContext context = null;
        TrustManager[] trustManagers = null;
        try {
            context = SSLContext.getInstance("TLS");
            trustManagers = getTrustManagers(certStr);
            context.init(null, trustManagers, null);
        } catch (Exception exp) {
            log.error("Unable to create SSLContext", exp);
            throw exp;
        }
        return context;
    }
	
	public static TrustManager[] getTrustManagers(String certificateStr) throws Exception {
	       KeyStore keyStore = null;
	       TrustManagerFactory factory = null;
	        try{
	            certificateStr = normaliseCertificate(certificateStr);
	            Certificate cert = createCertificate(certificateStr);
	            keyStore = keyStore.getInstance(KeyStore.getDefaultType());
	            keyStore.load(null);
	            String certAlias = USER_CERT_ALIAS + generateRandomNumber();
	            keyStore.setCertificateEntry(certAlias,cert);
	            factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	            factory.init(keyStore);
	        }catch (Exception exp){
	            log.error("Trustmanager configuration error",exp);
	            throw exp;
	        }
	        return factory.getTrustManagers();
	}

	private static String generateRandomNumber(){
        Random randomGenerator = new Random();
        return Integer.toString(randomGenerator.nextInt(9000));
    }
	
	// To remove extra spaces from the certificate string
	public static String normaliseCertificate(String inpString){
        String fPart = null;
        String mPart = null;
        String lPart = null;
        String outString = null;

        Pattern pattern = Pattern.compile("^-+[^-]+-+");
        Matcher matcher = pattern.matcher(inpString);

        if(matcher.find()){
            fPart = matcher.group();
        }
        String[] splitedParts = inpString.split("^-+[^-]+-+");
        String remainingPart = null;

        String[] parts = null;
        if(splitedParts.length == 2){
            remainingPart = splitedParts[1];
        }

        if(remainingPart != null){
            pattern = Pattern.compile("-+[^-]+-+$");
            matcher = pattern.matcher(remainingPart);
            if(matcher.find()){
                lPart = matcher.group();

                parts = remainingPart.split("-+[^-]+-+$");
                if(parts.length > 0)
                    mPart = parts[0];
            }
        }

        if(fPart!= null && mPart!= null && lPart!= null){
            mPart=mPart.replaceAll(" ", "");
            outString = fPart+"\n"+mPart+"\n"+lPart;
        } else{
            outString = null;
        }
        return outString;
    }

	public static Certificate createCertificate(String certificate){
        Certificate publicCert = null;
        ByteArrayInputStream inCert = new ByteArrayInputStream(certificate.getBytes());
        try{
        	publicCert = CertificateFactory.getInstance("X.509").generateCertificate(inCert);
        } catch (CertificateException e){
            log.error("Certificate creation error", e);
        }

        return publicCert;
    }

	/* Need to un-comment when test the ManageUsers service through this client
	public static void main(String args[]){
		
		String postBody = "<user userId=\"1131\"> <address>Kashi park</address> <firstName>amol</firstName> <lastName>ghongate</lastName><profile>Software Engineer</profile></user>";
		String putBody = "<user userId=\"1131\"> <address>Pimpale Gurav</address> <firstName>amol</firstName> <lastName>ghongate</lastName><profile>Srrrrrrrrrr software Engineer</profile></user>";
		HttpClient client = new HttpClient();
		client.executePost("tomcat", "tomcat", postBody);
		client.executeGet("tomcat", "tomcat", "1131");
		client.executePut("tomcat", "tomcat", putBody);
		client.executeDelete("tomcat", "tomcat", "1131");
		
	}*/
}
