package com.client;

import java.util.HashMap;
import java.util.Map;

public class Request {

	String url = null;
	Map<String, String> requestParam = new HashMap<String, String>();
	String body = null;
	String method = null;
	Map<String, String> postParams = new HashMap<String, String>();
	String certificatePath = null; // this is support for ssl, provide the public certificate that needs to import into the trustore of the SSLContext
	boolean isHttps = false;
	
	public String getUrl() {
		return url;
	}
	public Request setUrl(String url) {
		this.url = url;
		return this;
	}
	public Map<String, String> getRequestParam() {
		return requestParam;
	}
	public Request setRequestParam(Map<String, String> requestParam) {
		this.requestParam = requestParam;
		return this;
	}
	public String getBody() {
		return body;
	}
	public Request setBody(String body) {
		this.body = body;
		return this;
	}
	public String getMethod() {
		return method;
	}
	public Request setMethod(String method) {
		this.method = method;
		return this;
	}
	public Map<String, String> getPostParams() {
		return postParams;
	}
	public Request setPostParams(Map<String, String> postParams) {
		this.postParams = postParams;
		return this;
	}
	public String getCertificatePath() {
		return certificatePath;
	}
	public Request setCertificatePath(String certificatePath) {
		this.certificatePath = certificatePath;
		return this;
	}
	public boolean isHttps() {
		return isHttps;
	}
	public Request setHttps(boolean isHttps) {
		this.isHttps = isHttps;
		return this;
	}
	
	
}
