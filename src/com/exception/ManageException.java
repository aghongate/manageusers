package com.exception;

public class ManageException extends Exception{

	private static final long serialVersionUID = 1L;
	
	private String errorCode = null;
	
	public ManageException(){
		super();
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public ManageException(String message, Throwable throwable){
		super(message, throwable);
		extractErrorCode();
	}
	
	public ManageException(Throwable throwable){
		super(throwable);
		extractErrorCode();
	}
	
	public ManageException(Throwable throwable, String errorCode){
		super(throwable);
		this.errorCode = errorCode;
	}
	
	/*
	 * This is to handle a nested ManageException
	 */
	private void extractErrorCode(){
		Throwable t = this.getCause();	
		while(t != null){
			if (t instanceof ManageException){
				this.errorCode = ((ManageException) t).errorCode;
				return;
			}
			t.getCause();
		}
			
	}
	
}

