package com.rest;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import com.exception.ManageException;
import com.idm.Result;
import com.idm.User;
import com.service.UserObjectServiceImpl;
import com.service.init.Container;

@Path("/user")
public class RestUser {

	public static final String CREATE_USER = "CreateUser";
	public static final String UPDATE_USER = "UpdateUser";
	public static final String USER_SERVICE = "com.service.UserObjectServiceImpl";
	
	public static final int STATUS_OK = 200;
	public static final int STATUS_CREATED = 201;
	public static final int STATUS_FOUND = 302;
	public static final int STATUS_BAD_REQUEST = 400;
	public static final int STATUS_UNAUTHORISED = 401;
	public static final int STATUS_NOT_FOUND = 404;
	public static final int STATUS_INTERNAL_SERVER_ERROR = 500;
	
	private static Logger log = Logger.getLogger(RestUser.class); 
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response createUser(@Context HttpServletRequest request){
		return createOrUpdateUser(request, CREATE_USER);
	}
	
	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateUser(@Context HttpServletRequest request){
		return createOrUpdateUser(request, UPDATE_USER);
	}
	
	/* POST, PUT http body
	 * <user userId="1111">
	 *      <address>Indira nagar, Pune</address>
	 *      <firstName>amol</firstName>
	 *      <lastName>ghongate</lastName>
	 *      <profile>Engineer</profile>
	 *  </user>    
	 */
	public Response createOrUpdateUser(HttpServletRequest request, String op){

		User user = null;
		int status = STATUS_BAD_REQUEST;
		Result result = null;
		
		try{
			user = this.createObject(request);
			
			if (user != null && (op.equals(CREATE_USER)) ? 
					((UserObjectServiceImpl)Container.getService(USER_SERVICE)).createUser(user) : 
					((UserObjectServiceImpl)Container.getService(USER_SERVICE)).updateUser(user)){
				result = new Result("true", op.equals(CREATE_USER) ? "USER_CREATED" : "USER_UPDATED", "User with userId [" + user.getUserId() + "] " + op + " successfully");
				status = STATUS_CREATED;
			} else {
				result = new Result("false", "XML_VALIDATION_FAILED", Result.XML_VALIDATION_FAILED);
				status = STATUS_BAD_REQUEST;
			}
			
		} catch(Exception e){
			if (e.getCause() instanceof org.hibernate.exception.ConstraintViolationException){
				log.error("Duplicate user : " + user.getUserId());
				return createResponse(STATUS_BAD_REQUEST, new Result("false", "DUPLICATE_USER", Result.DUPLICATE_USER));
				
			}
			log.error("XML validation failed");
			return createResponse(STATUS_BAD_REQUEST, new Result("false", "XML_VALIDATION_FAILED", Result.XML_VALIDATION_FAILED));
		}
		
		log.debug("Successfully create user : " + user.getUserId());
		return createResponse(status, result);
		
	}
	
	/*
	 * This is to make sure that the input xml from the client is verified against the xsd schema
	 */
	private User createObject(HttpServletRequest request) throws Exception {
		    User user = null;
					
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(this.getClass().getResource("UserSchema.xsd").toURI()));
			JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			unmarshaller.setSchema(schema);
			
			user = (User) unmarshaller.unmarshal(request.getInputStream());
		
		return user;
		
	}
	
	@DELETE
	@Path("/delete/{userId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response deleteUser(@PathParam("userId")String userId){
		boolean isExists = false;
		if (userId != null && !userId.isEmpty()){
			try {
				isExists = ((UserObjectServiceImpl)Container.getService(USER_SERVICE)).deleteUserById(userId);
			} catch (ManageException e) {
				return createResponse(STATUS_INTERNAL_SERVER_ERROR, new Result("false", "FAILED_DELETE_USER", "Filed to delete user with userId [" + userId + "] from the system."));
			}
		}
		if (isExists){
			log.debug("User deleted from the system with userid : " + userId);
			return createResponse(STATUS_OK, new Result("true", "DELETE_USER", "User with userId [" + userId + "] deleted from the system."));
		} else {
			log.debug("User does not exists in the system");
			return createResponse(STATUS_OK, new Result("false", "DELETE_USER_NOT_EXISTS", "User with userId [" + userId + "] does not exists in the system."));
		}

	}
	
	@GET
	@Path("/get/{userId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getUser(@PathParam("userId")String userId){
		
		if (userId == null || userId.isEmpty()){
			return createResponse(STATUS_UNAUTHORISED, new Result("false", "MISSING_USER_ID", Result.MISSING_USER_ID));
		}
		User user = null;
		String errorMsg = "User with user id [" + userId + "] not found.";
		try{
			user = ((UserObjectServiceImpl)Container.getService(USER_SERVICE)).getUserById(userId);
		} catch(ManageException e){
			log.error("User not found");
			return createResponse(STATUS_NOT_FOUND, new Result("false", "USER_NOT_FOUND", errorMsg));
		}
		if (user != null){
			log.info("User found");
			return createResponse(STATUS_FOUND, user);
		} else{
			log.debug("User not found");
			return createResponse(STATUS_NOT_FOUND, new Result("false", "USER_NOT_FOUND", errorMsg));
		}
		

	}
	
	private Response createResponse(int status, Object errorMsg){
		return Response.status(status).entity(errorMsg).build();
	}
}
