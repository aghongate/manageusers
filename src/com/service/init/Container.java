package com.service.init;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.exception.ManageException;
import com.service.ManageService;

public class Container {

	static ServletConfig config = null;
	static ServletContext context = null;
	static Container container = null;
	static Map<String, ManageService> registeredServices = new HashMap<String, ManageService>();
	
	Container(ServletConfig config, ServletContext context){
		Container.config = config;
		Container.context = context;
	}
	
	public static void initialize(ServletConfig config, ServletContext context){
		container = new Container(config, context);	
	}
	
	/*
	 * Read the manage-services.xml file and extract the service name.
	 * Then create an object of that service and perform registration
	 */
	public void initServices() throws ManageException{
		String configString = config.getInitParameter("configFile");
		String configPath = context.getRealPath(configString);
		
		File configFile = new File(configPath);
		
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(configFile);
			doc.getDocumentElement().normalize();
			NodeList serviceList = doc.getElementsByTagName("manageService");
			for (int position = 0; position<serviceList.getLength(); position++){
				Node service = serviceList.item(position);
				Element element = (Element)service;
				NodeList serviceParam = element.getElementsByTagName("service-class");
				String serviceClass = ((Element)serviceParam.item(0)).getAttribute("name"); 
				//System.out.println("class name : " + serviceClass + " and length is : " + serviceParam.getLength());
				
				String[] parameters = new String[serviceParam.getLength()];
				
				for (int i=0 ; i<serviceParam.getLength(); i++){
					Node node = serviceParam.item(i);
					Element nodeElement = (Element)node;
		            parameters[i] = nodeElement.getElementsByTagName("service-property").item(i).getTextContent();
					//System.out.println("node name : " + parameters[i]);
				}		
				registerService(serviceClass, parameters);
		
			}		
		} catch(Exception e){
			//System.out.println("Error while initializing the services.");
			throw new ManageException("Error while initializing the services.", e);
		}
		
	}
	
	/*
	 * Service gets initialized at the start of the web server. 
	 */
	private void registerService(String serviceClass, String[] parameters) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		ManageService service = (ManageService)Class.forName(serviceClass, true, Thread.currentThread().getContextClassLoader()).newInstance();
		registeredServices.put(serviceClass, service);
	}
	
	public static ManageService getService(String serviceName){
		return registeredServices.get(serviceName);
	}
	
}
