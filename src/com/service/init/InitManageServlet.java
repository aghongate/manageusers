package com.service.init;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.exception.ManageException;

public class InitManageServlet extends HttpServlet{

	Container container = null;
	public void init(ServletConfig sc) throws ServletException {
		try{
			super.init(sc);
			container = new Container(sc, getServletContext());
			container.initServices();
		
		} catch (ManageException me){
			throw new ServletException("Unable to registered the service", me);
		}
	}

}
