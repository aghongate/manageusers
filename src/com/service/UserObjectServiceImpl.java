package com.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.exception.ManageException;
import com.idm.User;
import com.storage.PersistanceManager;

public final class UserObjectServiceImpl extends AbstractManageService implements UserObjectService{

	private Properties property = null;
	private static Logger log = Logger.getLogger(UserObjectServiceImpl.class);
	
	@Override
	public void initialize(Properties properties) throws ManageException {
		this.property = properties;		
	}

	@Override
	public Properties getProperties() throws ManageException {
		return this.property;
	}

	@Override
	public void start() throws ManageException {
		
	}

	@Override
	public boolean createUser(User user) throws ManageException {
		if (user != null){
			PersistanceManager.getInstance().createOrUpdate(user);
		}
		return true;
	}

	@Override
	public boolean updateUser(User user) throws ManageException {
		String hql = "SELECT u.id from User u WHERE u.userId = " + user.getUserId();
		String id = (String)PersistanceManager.getInstance().executeQuery(hql).get(0);
		user.setId(id);
		PersistanceManager.getInstance().createOrUpdate(user);
		return true; 
	}

	@Override
	public User getUserById(String userId) throws ManageException {
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("userId", userId);
		List users = PersistanceManager.getInstance().execiteCriteria(User.class, criteria);
		if (users.size() > 0){
			return (User)users.get(0);
		} else {
			return null;
		}
	}

	@Override
	public boolean deleteUserById(String userId) throws ManageException {
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("userId", userId);
		List listOfUsers = PersistanceManager.getInstance().execiteCriteria(User.class, criteria);
		if (listOfUsers.size() != 0){
			User user = (User) listOfUsers.get(0);
			if (user != null){
				PersistanceManager.getInstance().delete(user);
				return true;
			}
		}
		
		return false;
	}

}
