package com.service;

import com.exception.ManageException;
import com.idm.User;

public interface UserObjectService{

	public boolean createUser(User user) throws ManageException;
	public boolean updateUser(User user) throws ManageException;
	public User getUserById(String userId) throws ManageException;
	public boolean deleteUserById(String userId) throws ManageException;
	
}
