package com.idm;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.storage.PersistanceObject;

@XmlRootElement(name = "user")
public class User extends PersistanceObject {
	
	String id;
	String userId;
	String firstName;
	String lastName;
	String profile;
	String address;
	
	@XmlTransient
	public String getId() { 
		return id;
	}

	
	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}
	
	@XmlAttribute
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	
	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfile() {
		return profile;
	}
	
	@XmlElement
	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	@XmlElement
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public boolean equals(Object obj){
		if (this == obj){
			return true;
		} else if (obj instanceof User){
			if (((User)obj).id.equals(this.id)){
				return true;
			} else if (((User)obj).userId.equals(this.userId)){
				return true;
			}
		}
		
		return false;

	}

}
