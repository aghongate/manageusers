package com.idm;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "result")
public class Result {

	public final static String DUPLICATE_USER="User already exists";
	public final static String USER_CREATED="User created in the system";
	public final static String USER_DELETED="User deleted from the system";
	public final static String USER_NOT_FOUND="User not found in the system";
	public final static String XML_VALIDATION_FAILED="XML input values exceeded the limit of characters. The number of characters for FirstName, LastName, Address, Profile should not exceed 20";
	public final static String MISSING_USER_ID = "User id is missing in the request";
	public final static String USER_FOUND = "User found in the system";
	
	String success = null;
	String errorCode = null;
	String message = null;
	
	Result(){
	}
	
	public Result(String success, String errorCode, String message){
		this.success = success;
		this.errorCode = errorCode;
		this.message = message;
	}
	
	@XmlElement
	public String getSuccess() {
		return success;
	}
	
	public void setSuccess(String success) {
		this.success = success;
	}
	
	@XmlElement
	public String getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	@XmlElement
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
}
